==============================================================
Amil - Django project using Celery
==============================================================


Installing requirements
=======================

    $ pip install -r requirements.txt

Starting the worker
===================

    $ celery -A proj worker -l info

Running a task
===================

    $ python ./manage.py shell
    from amil.tasks import *
    res = add_user.delay({u'transaction_number':u'123', u'name':u'JonyBravo'})
    res.get()

FROM python:3.6

ADD ./app/requirements.txt /app/requirements.txt
ADD ./app/ /app/

WORKDIR /app/

ENTRYPOINT app/run.sh
